create bitmap index actpro
on actvars(ProdLevel.code_level) from actvars,Prodlevel where
actvars.PRODUCT_LEVEL=ProdLevel.CODE_LEVEL;

create bitmap index actcust
on actvars(CUSTLEVEL.STORE_LEVEL) from actvars,CUSTLEVEL where
actvars.CUSTOMER_LEVEL=CUSTLEVEL.STORE_LEVEL;


create bitmap index acttim
on actvars(TIMELEVEL.TID) from actvars,TIMELEVEL where
actvars.TIME_LEVEL=TIMELEVEL.TID;

create bitmap index actchan
on actvars(CHANLEVEL.BASE_LEVEL) from actvars,CHANLEVEL where
actvars.CHANNEL_LEVEL=CHANLEVEL.BASE_LEVEL;

